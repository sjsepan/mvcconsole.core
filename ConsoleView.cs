﻿#define USE_CUSTOM_VIEWMODEL
// #define DEBUG_MODEL_PROPERTYCHANGED
// #define DEBUG_SETTINGS_PROPERTYCHANGED

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using MvcLibrary.Core;

namespace MvcConsole.Core
{
    public class ConsoleView :
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;
#if USE_CUSTOM_VIEWMODEL
        protected MVCConsoleViewModel ViewModel;
#else
        protected ConsoleViewModel<string, MVCSettings, MVCModel> ViewModel = 
            default(ConsoleViewModel<string, MVCSettings, MVCModel>);
#endif
        //private static bool _ValueChangedProgrammatically;
        #endregion Declarations

        #region Constructors
        public ConsoleView()
        {
            try
            {
                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.writeLineWrapperOutputDelegate;

                //subscribe to notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region IDisposable
        ~ConsoleView()
        {
            Dispose(false);
        }

        public virtual void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            // GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    //unsubscribe from model notifications
                    if (PropertyChanged != null)
                    {
                        PropertyChanged -= PropertyChangedEventHandlerDelegate;
                    }
                }
                // dispose unmanaged resources
                disposed = true;
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region PropertyChangedEventHandlerDelegate
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
#region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                else if (e.PropertyName == "Progress")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("{0}", Progress));
                }
                if (e.PropertyName == "StatusMessage")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("{0}", ViewModel.StatusMessage));
                    e = new PropertyChangedEventArgs(e.PropertyName + ".handled");
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("{0}", ViewModel.ErrorMessage));
                    e = new PropertyChangedEventArgs(e.PropertyName + ".handled");
                }
                //Note: not databound, so handle event
                else if (e.PropertyName == "SomeInt")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeInt: {0}", ModelController<MVCModel>.Model.SomeInt));
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeBoolean: {0}", ModelController<MVCModel>.Model.SomeBoolean));
                }
                else if (e.PropertyName == "SomeString")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeString: {0}", ModelController<MVCModel>.Model.SomeString));
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeOtherInt: {0}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt));
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeOtherBoolean: {0}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean));
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeOtherString: {0}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                }
                else if (e.PropertyName == "SomeComponent")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("StillAnotherInt: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt));
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("StillAnotherBoolean: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean));
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("StillAnotherString: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                }
                else if (e.PropertyName == "StillAnotherComponent")
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                }
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                        ConsoleApplication.DefaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
#endregion Model

#region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
#if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.DefaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
#endif
                }
#endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
#endregion PropertyChangedEventHandlerDelegate

#region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }

        private int _Progress;
        public int Progress
        {
            get { return _Progress; }
            set
            {
                _Progress = value;
                OnPropertyChanged(nameof(Progress));
            }
        }
#endregion Properties

#region Methods
#region ConsoleAppBase
        protected void InitViewModel()
        {
            FileDialogInfo<object, string> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                InitModelAndSettings();

                //settings used with file dialog interactions
                settingsFileDialogInfo =
                    new FileDialogInfo<object, string>
                    (
                        parent: this,
                        modal: true,
                        title: null,
                        response: null,
                        newFilename: SettingsController<MVCSettings>.FILE_NEW,
                        filename: null,
                        extension: SettingsBase.FileTypeExtension,
                        description: SettingsBase.FileTypeDescription,
                        typeName: SettingsBase.FileTypeName,
                        additionalFilters: [
                            "MvcSettings files (*.mvcsettings)|*.mvcsettings",
                            "JSON files (*.json)|*.json",
                            "XML files (*.xml)|*.xml",
                            "All files (*.*)|*.*"
                        ],
                        multiselect: false,
                        initialDirectory: default,
                        forceDialog: false,
                        forceNew: false,
                        customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    )
                    {
                        //set dialog caption
                        Title = Program.APP_NAME
                    };

#if USE_CUSTOM_VIEWMODEL
                //class to handle standard behaviors
                ViewModelController<string, MVCConsoleViewModel>.New
                (
                    ViewName,
                    new MVCConsoleViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, string>()
                        {
                            { "New", "New" },
                            { "Save", "Save" },
                            { "Open", "Open" },
                            { "Print", "Print" },
                            { "Copy", "Copy" },
                            { "Properties", "Properties" }
                        },
                        settingsFileDialogInfo
                    )
                );

                //select a viewmodel by view name
                ViewModel =
                    ViewModelController<string, MVCConsoleViewModel>.ViewModel[ViewName];
#else
                //class to handle standard behaviors
                ViewModelController<string, ConsoleViewModel<string, MVCSettings, MVCModel>>.New
                (
                    ViewName,
                    new ConsoleViewModel<string, MVCSettings, MVCModel>
                    (
                        this.PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, string>() 
                        { 
                            { "New", "New" }, 
                            { "Save", "Save" },
                            { "Open", "Open" },
                            { "Print", "Print" },
                            { "Copy", "Copy" },
                            { "Properties", "Properties" }
                        },
                        settingsFileDialogInfo
                    )
                );
                
                //select a viewmodel by view name
                ViewModel = 
                    ViewModelController<string, ConsoleViewModel<string, MVCSettings, MVCModel>>.ViewModel[ViewName];
#endif

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || (SettingsController<MVCSettings>.Filename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            //save user and application settings 
            //Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                // SettingsBase.SerializationFormat serializationFormat = MVCSettings.SerializeAs;
                // string settingsFilename = SettingsController<MVCSettings>.Filename;

                // //convert format on save; use when testing new format, like XML -> JSON
                // MVCSettings.SerializeAs = SettingsBase.SerializationFormat.Json;
                // SettingsController<MVCSettings>.Filename = "steve.json.mvcsettings";

                //SAVE
                ViewModel.FileSave();

                // // reset
                // MVCSettings.SerializeAs = serializationFormat;
                // SettingsController<MVCSettings>.Filename = settingsFilename;

            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        public int Main2()
        {
            int returnValue = -1; //default to fail code

            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                //init settings

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                Run();

                ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);

                DisposeSettings();

                ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = string.Format("{0} did NOT complete: '{1}'", ViewName, ViewModel.ErrorMessage);
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                ViewModel = null;
            }
            return returnValue;
        }

        protected void Run()
        {
            ViewModel.DoSomething();
        }
#endregion ConsoleAppBase

#region Utility
        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void ApplySettings()
        {
            try
            {
                //_ValueChangedProgrammatically = true;

                //apply settings that have databindings
                //BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Console.Title = Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                //ViewModel.StatusBarDirtyMessage.Visible = (SettingsController<Settings>.Settings.Dirty);

                //_ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;
            try
            {
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }
#endregion Utility
#endregion Methods
    }
}
