# readme.md - README for MvcConsole.Core v0.7

## About

Desktop console app demo, on Linux, in C# DotNet [5|6]; requires mvclibrary.core, ssepan.application.core.terminal, ssepan.application.core, ssepan.io.core, ssepan.utility.core

### Purpose

To illustrate a working Desktop Console app in c# on .Net (Core).

![MvcConsole.Core.png](./MvcConsole.Core.png?raw=true "Screenshot")

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Enhancements

1.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

0.7:
~Set new Website property in AssemblyInfo, defined in AssemblyInfoBase and used in viewmodel.

0.6:
~Changes to client app resulting from refactoring of Ssepan.Application.Core[.*].
~Added this readme.md.
~Added license.txt file.

0.5:
~Convert Ssepan.Core.*libs to use StdErr for logging, and replace System.Diagnostics.EventLogEntryType with own enum.
~Convert Mvc* projects to use log changes, passing new enum instead of one from System.Diagnostics.
~Clean up usings in all projects.

0.4:
~Convert MVCConsole app and library, along with support libraries, to Core.

Steve Sepan
<sjsepan@yahoo.com>
3/4/2024
